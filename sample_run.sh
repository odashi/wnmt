#!/bin/bash
# To install prerequisites, run './install_all.sh'
# To inspect the system, run './inspector.sh args...'

# Sample (run by root user)
tar xf sample.tar.gz
./install_all.sh
./inspector.sh wnmt2018 organizer echo sample/wnmt2018_organizer_echo.image sample/test.src sample/test.trg sample_results --cpu 0 --memory 128m
#./inspector.sh wnmt2018 organizer echo sample/wnmt2018_organizer_echo.image sample/test.src sample/test.trg sample_results --cpu 0,1,2,3 --gpu --memory 16g
