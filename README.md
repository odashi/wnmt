Usage
-----

    git clone <this_repo>.git
    cd this_repo
    sudo ./sample_run.sh # if no problem this finally generates BLEU/NIST scores.

See sample_run.sh for detail.
