#!/bin/bash

apt update
apt install -y build-essential cmake docker.io libboost-dev libboost-program-options-dev libboost-test.dev
tar xf mteval-1.0.1.tar.gz
cd mteval-1.0.1
mkdir build
cd build
cmake ..
make
