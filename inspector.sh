#!/bin/bash -e

usage_exit() {
  echo "usage: $0 <workshop_id> <team_id> <system_id> <image_file_path> <src_file_path> <trg_file_path> <result_directory> [--cpu id,id,...] [--gpu] [--memory <size>]"
  echo "example: $0 wnmt2018 organizer cat /path/to/the_docker_image test.src test.trg results --cpu 0,1,2,3 --gpu --memory 4g"
  exit 1
}

if [ $# -lt 7 ]; then usage_exit; fi

workshop_id=$1; shift; echo "inspector: info: workshop_id: ${workshop_id}"
team_id=$1; shift; echo "inspector: info: team_id: ${team_id}"
system_id=$1; shift; echo "inspector: info: system_id: ${system_id}"
image_file_path=$1; shift; echo "inspector: info: image_file_path: ${image_file_path}"
src_file_path=$1; shift; echo "inspector: info: src_file_path: ${src_file_path}"
trg_file_path=$1; shift; echo "inspector: info: trg_file_path: ${trg_file_path}"
result_directory=$1; shift; echo "inspector: info: result_directory: ${result_directory}"

opt_cpu="--cpuset-cpus=0"
opt_gpu=false
opt_nvidia=""
opt_memory="--memory=16g"

while [ $# -gt 0 ]; do
  case $1 in
    --cpu)
      shift
      if [ $# -eq 0 ]; then usage_exit; fi
      opt_cpu="--cpuset-cpus=$1"
      ;;
    --gpu)
      opt_gpu=true
      opt_nvidia="--runtime=nvidia"
      ;;
    --memory)
      shift
      if [ $# -eq 0 ]; then usage_exit; fi
      opt_memory="--memory=$1"
      ;;
    *)
      ;;
  esac
  shift
done

echo "inspector: info: opt_cpu: ${opt_cpu}"
echo "inspector: info: opt_gpu: ${opt_gpu}"
echo "inspector: info: opt_memory: ${opt_memory}"

if [ -e ${result_directory} ]; then
  echo "Directory '${result_directory}' already exists."
  exit 1
fi
mkdir -p ${result_directory}

image_name="${workshop_id}_${team_id}_${system_id}"

echo "inspector: proc: Obtaining image size ..."
image_file_size=$(ls -l ${image_file_path} | cut -d ' ' -f 5)
echo ${image_file_size} > ${result_directory}/image_file_size
echo "inspector: info: image_file_size: ${image_file_size}"

echo "inspector: proc: Loading image ..."
docker load -i ${image_file_path} > /dev/null

if ${opt_gpu}; then
  echo "inspector: proc: Launching nvidia-smi ..."
  nvidia-smi --query-gpu=memory.used --format=csv,noheader,nounits -l 1 -f ${result_directory}/gpumem_usage_history &
  nvidia_smi_pid=$!
fi

echo "inspector: proc: Launching container ..."
container_id=$(docker run -itd ${opt_cpu} ${opt_memory} --memory-swap=0 ${opt_nvidia} ${image_name} /bin/sh)
echo "inspector: info: container_id: ${container_id}"

echo "inspector: proc: Copying src file ..."
docker exec ${container_id} mkdir /${workshop_id}_data
docker cp ${src_file_path} ${container_id}:/${workshop_id}_data/src

echo "inspector: proc: Running translator ..."
docker exec ${container_id} sh /run.sh /${workshop_id}_data/src /${workshop_id}_data/hyp

echo "inspector: proc: Copying hyp file ..."
docker cp ${container_id}:/${workshop_id}_data/hyp ${result_directory}/hyp

echo "inspector: proc: Obtaining CPU usage ..."
cpu_usage=$(cat /sys/fs/cgroup/cpuacct/docker/${container_id}/cpuacct.usage)
echo ${cpu_usage} > ${result_directory}/cpu_usage
echo "inspector: info: cpu_usage(ns): ${cpu_usage}"

echo "inspector: proc: Obtaining memory usage ..."
max_memory_usage=$(cat /sys/fs/cgroup/memory/docker/${container_id}/memory.max_usage_in_bytes)
echo ${max_memory_usage} > ${result_directory}/max_memory_usage
echo "inspector: info: max_memory_usage(byte): ${max_memory_usage}"

if ${opt_gpu}; then
  echo "inspector: proc: Terminating nvidia-smi ..."
  kill -SIGINT ${nvidia_smi_pid}
  kill_status=$?
  while [ ${kill_status} -eq 0 ]; do
    kill_status=0
    kill -0 ${nvidia_smi_pid} 2> /dev/null || kill_status=$?
  done

  echo "inspector: proc: Obtaining GPU memory usage ..."
  max_gpumem_usage=$(awk 'BEGIN { m = 0; } { if ($1 > m) m = $1; } END { print m; }' ${result_directory}/gpumem_usage_history)
  echo ${max_gpumem_usage} > ${result_directory}/max_gpumem_usage
  echo "inspector: info: max_gpumem_usage(MB): ${max_gpumem_usage}"
fi

echo "inspector: proc: Removing Docker image and container ..."
docker rm -f ${container_id} > /dev/null
docker rmi -f ${image_name} > /dev/null

echo "inspector: proc: Calculating MT metrics ..."
./mteval-1.0.1/build/bin/mteval-corpus -r ${trg_file_path} -h ${result_directory}/hyp -e BLEU NIST --output-stats | tr '\t' '\n' > ${result_directory}/mt_metrics
bleu_score=$(grep '^BLEU=' ${result_directory}/mt_metrics | cut -d '=' -f 2)
echo ${bleu_score} > ${result_directory}/bleu
echo "inspector: info: BLEU: ${bleu_score}"
nist_score=$(grep '^NIST=' ${result_directory}/mt_metrics | cut -d '=' -f 2)
echo ${nist_score} > ${result_directory}/nist
echo "inspector: info: NIST: ${nist_score}"
